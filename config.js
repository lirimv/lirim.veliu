module.exports = {
  siteTitle: 'Lirim Veliu', // <title>
  manifestName: 'Lirim Veliu',
  manifestShortName: 'Landing', // max 12 characters
  manifestStartUrl: '/',
  manifestBackgroundColor: '#663399',
  manifestThemeColor: '#663399',
  manifestDisplay: 'standalone',
  manifestIcon: 'src/assets/img/website-icon.png',
  pathPrefix: `/lirim-veliu/`, // This path is subpath of your hosting https://domain/portfolio
  authorName: 'Lirim Veliu',
  heading: 'Web Developer',
  // social
  socialLinks: [
    {
      icon: 'fa-phone',
      name: 'Phone',
      url: 'tel:+4917660411082',
    },
    {
      icon: 'fa-envelope-o',
      name: 'Email',
      url: 'mailto:lirim@veliu.net',
    },
  ],
};
